(import '(java.util Scanner))
(println "introduce el tiempo en minutos ")
(def scan (Scanner. *in*))
(def tiempo(.nextInt scan))

(def dias(quot tiempo 1440))
(def temporal(mod tiempo 1440))
(def horas(quot temporal 60))
(def minutos(mod temporal 60))


(println "Dias: " dias " --- " "Horas: " horas " --- " "Minutos: " minutos)
