(import '(java.util Scanner))
(def scan (Scanner. *in*))
(println "operacion deceada 1 sumar, 2 restar, 3 producto, 4 division: ")
(def operando (.nextInt scan))

(if (= operando 1) 
  (do
 (println "introduce los numeros a sumar: ")
 	 (def a (.nextInt scan))
 	 (def b (.nextInt scan))
 	 (def resu (+ a b))
(println "la suma es  :" resu)
  	))

(if (= operando 2) 
  (do
 (println "introduce los numeros a restar: ")
 	 (def a (.nextInt scan))
 	 (def b (.nextInt scan))
 	 (def resu (- a b))
(println "la resta es  :" resu)
  	))

(if (= operando 3) 
  (do
 (println "introduce los numeros a multiplicar : ")
 	 (def a (.nextInt scan))
 	 (def b (.nextInt scan))
 	 (def resu (* a b))
(println "la producto es  :" resu)
  	))

(if (= operando 4) 
  (do
 (println "introduce los numeros a dividir: ")
 	 (def a (.nextInt scan))
 	 (def b (.nextInt scan))
 	 (def resu (/ a b))
(println "la division es  :" resu)
  	))