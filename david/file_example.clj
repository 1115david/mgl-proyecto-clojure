(import '(java.util Scanner))
(use 'clojure.java.io)
(use '[clojure.string :only (join split)])

;;Funcion Leer archivo
(defn Leer[x]
	(with-open [rdr (reader (str "/" x))]
  		(doseq [line (line-seq rdr)]
     		(println(join "."(split line #",")))
     	) 
 	)

)
;;Fin Funcion

;;Funcion para escribir en el archivo
(defn Escribir[y z]
(with-open [wrtr(writer (str "/" y ))]
(.write wrtr (str z )))
)
;;Fin Funcion
;***********************************************************************************************
(defn menu[numero ruta]
  (if (== numero 1) 
  	(do 
  		(println "Introduce el nuevo texto a agregar al archivo: ")
        (def scan (Scanner. *in*))
        (def valNUEVO(.next scan))
        (Escribir ruta valNUEVO)
  	)
  )
  
  (if (== numero 2) 
  	(do
  	    (println "bye...") 
  	)
  )
)

(println "Introduce la ruta del archivo txt omitiendo C:/ ")
(def scan (Scanner. *in*))
(def varRUTA(.next scan))

(println " ")
(println "Dentro del archivo se encontro: ")
(println " ")
(Leer varRUTA)

(println "Quieres reescribir el archivo ? 1)si 2)no")

(def scan (Scanner. *in*))
(def valOPCION(.nextInt scan))
(menu valOPCION varRUTA)
