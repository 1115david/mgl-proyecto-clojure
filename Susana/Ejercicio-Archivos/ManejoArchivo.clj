(import '(java.util Scanner))
(use 'clojure.java.io) 
(use '[clojure.string :only (join split)])
(def scan (Scanner. *in*))
(def op1 "1")

(println "---MANEJO  DE ARCHIVOS---")
(println "--OPCIONES--")
(println "1) Leer el Archivo")
(println "2) Agregar al Archivo")

(def opcion (.next scan))

(defn leerA []
	(with-open [rdr (reader "C:\\Users\\SUSANA\\repositorio_codigo\\mgl-proyecto-clojure\\Susana\\Ejercicio-Archivos\\archi.txt" :append true)]
  		(doseq [line (line-seq rdr)]
     		(println(join "|"(split line #","))) ))
)

(if (.exists (as-file "C:\\Users\\SUSANA\\repositorio_codigo\\mgl-proyecto-clojure\\Susana\\Ejercicio-Archivos\\archi.txt"))
		(println "Archivo existente")
		(spit "C:\\Users\\SUSANA\\repositorio_codigo\\mgl-proyecto-clojure\\Susana\\Ejercicio-Archivos\\archi.txt" "")
	)

(defn agregarT []
	(println "Escribir en el siguiente orden")
	(println "Procesador|RAM|DD|SO|Tipo Computadora")
	(def texto (.nextLine scan))
	(with-open [wrtr (writer "C:/repositorio_codigo/mgl-proyecto-clojure/irving/archivos/archivo.txt" :append true)](.write wrtr texto) (.newLine wrtr))
)

(if (= opcion op1) 
	(do 
	(println "--Computadoras agregadas--")
		(leerA) )
	)
(if (= opcion "2")
		(println "Opcion 2")
	)
