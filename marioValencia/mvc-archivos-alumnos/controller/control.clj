(ns controller.control
	(:require [view.vista :refer [ mensajeInicial menu eleccion encuenstaAlumno VDatosAlumno]])
	(:require [model.modelo :refer [ constructorAlumno ModEntidadAlumno]])
  );este  es un comentario tmabien, se crea el nameespace y se indica  con rquire  que  otro namespace se utilizara

(use 'clojure.java.io)
(use '[clojure.string :only (join split)])
(use '[clojure.string :only (split triml)])


(def incremento 0)
(def arregloAlmnos [])

(def bn "hola")
(defn bienvenida[] (def bn (str "Bienvenido")))
(mensajeInicial bn)

(menu)

(defn llenarConstructorAlumno[x]
	(constructorAlumno (get x 0) (get x 1) (get x 2) (get x 3) (get x 4))
	;(println ModEntidadAlumno)
	(def arregloAlmnos (assoc arregloAlmnos incremento ModEntidadAlumno))
	;(println arregloAlmnos)
)

(defn leerAlumnos[x]
	(with-open [rdr (reader (str "/Users/mario/repositorio_codigo/mgl-proyecto-clojure/marioValencia/mvc-archivos-alumnos/" x))]
  		(doseq [line (line-seq rdr)]
     		(println (join "|"(split line #",")))
     		(def linea (str line))
     		;(def v (assoc v incremento line))
     		 ;(def incremento (+ incremento 1))
     		 ;(println linea "desde")
     		 (def lineaAlumno (split linea #","))
     		 ;(println lineaAlumno)
     		 (llenarConstructorAlumno lineaAlumno)
     		  (def incremento (+ incremento 1))
     		
     	) 
 	)

)

(defn registrarAlumno[ruta] 
(with-open [wrtr(writer (str "/Users/mario/repositorio_codigo/mgl-proyecto-clojure/marioValencia/mvc-archivos-alumnos/" ruta ))]
;(println "esto se sobrescribira: " arreglo)
(def lim (count arregloAlmnos))
;(println lim)
(def i 0)
(while (< i lim)
;	(println (nth arreglo i))
	;(.write wrtr (str (ModEntidadAlumno :noControl)","(ModEntidadAlumno :nombre)","(ModEntidadAlumno :carrera)","(ModEntidadAlumno :semestre)","(ModEntidadAlumno :sexo) "\r\n"))
	(.write wrtr (str 
		((get arregloAlmnos i) :noControl)","
		((get arregloAlmnos i) :nombre)","
		((get arregloAlmnos i) :carrera)","
		((get arregloAlmnos i) :semestre)","
		((get arregloAlmnos i) :sexo) "\r\n"))
	(def i (+ i 1))
	)
)
)



(defn recolectarDatosAlumno[]
	(leerAlumnos "file.txt")
	(encuenstaAlumno)
	(constructorAlumno 
		(get VDatosAlumno 0) 
		(get VDatosAlumno 1) 
		(get VDatosAlumno 2) 
		(get VDatosAlumno 3) 
		(get VDatosAlumno 4)
	)
	(def arregloAlmnos (assoc arregloAlmnos incremento ModEntidadAlumno)) ;se agrega al arreglo el ultimo alumno capturado
	(registrarAlumno "file.txt")
	(println "Registro grabado ... :)")
)


(defn tarea[x]
	(if (= x "1") 
		(recolectarDatosAlumno)
		(if (= x "2") (leerAlumnos "file.txt") (println "Salir"))
    )
)


(tarea eleccion)

